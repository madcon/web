## Source https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db#file-makefile-L1

APP = madcon-web
PORT = 3000

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help


build: ## Build the container
	docker build -f dockerbuild/Dockerfile -t $(APP) .

css: ## Minify css
	purgecss --css css/* --content index.html  --output css/pure/

run: ## Run
	docker rm -f $(APP)
	docker run -p $(PORT):80 -d --name $(APP)  $(APP):latest

up: build run ## Run container on port configured
